# Azure Debian image testing

These image tests rely heavily on [az-cli-helpers (Salsa)](https://salsa.debian.org/trstringer/az-cli-helpers) and must be on the test machine for these tests to work.

## Test matrix

| Image | cloud-init version | cloud-init pkg version | Azure agent version | Azure agent pkg version | Test script | Output | Test type |
| ----- | ------------------ | ---------------------- | ------------------- | ----------------------- | ----------- | ------ | ----- |
| debian:debian-10:10:0.20200511.260 | 20.2 | 20.2-2 (testing) | 2.2.45 | 2.2.45-4~deb10u1 (stable) | [Script](./buster/validate_debian_debian-10_10_0.20200511.260_with_cloud-init_20.2-2.sh) | [Output](./buster/validate_debian_debian-10_10_0.20200511.260_with_cloud-init_20.2-2.out) | Common |
| debian:debian-10:10:0.20200511.260 | 20.2 | 20.2-2 (testing) | 2.2.45 | 2.2.45-4~deb10u1 (stable) | [Script](./buster/sru_validate_debian_debian-10_10_0.20200511.260_with_cloud-init_20.2-2.sh) | [Output](./buster/sru_validate_debian_debian-10_10_0.20200511.260_with_cloud-init_20.2-2.out) | SRU |
| debian:debian-10:10:0.20200511.260 | 20.2 | 20.2-2~bpo10+1 (buster-backports) | 2.2.45 | 2.2.45-4~deb10u1 (stable) | [Script](./buster/validate_debian_debian-10_10_0.20200511.260_with_cloud-init_20.2-2_bp.sh) | [Output](./buster/validate_debian_debian-10_10_0.20200511.260_with_cloud-init_20.2-2_bp.out) | Common |
| debian:debian-10:10:0.20200511.260 | 20.2 | 20.2-2~bpo10+1 (buster-backports) | 2.2.45 | 2.2.45-4~deb10u1 (stable) | [Script](./buster/sru_validate_debian_debian-10_10_0.20200511.260_with_cloud-init_20.2-2_bp.sh) | [Output](./buster/sru_validate_debian_debian-10_10_0.20200511.260_with_cloud-init_20.2-2_bp.out) | SRU |

*Note: SRU testing is a list of tests that the cloud-init team runs against Ubuntu, and that I have modified to run against Debian for additional, thorough testing.*
