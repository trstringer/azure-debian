#!/bin/bash -i

set -e

IMAGE="debian:debian-10:10:0.20200511.260"
NETCFG="/etc/network/interfaces.d/50-cloud-init"
KNOWN_HOSTS="/home/trstringer/.ssh/known_hosts"

OUTPUT=$(az_vm_create "$IMAGE" "$IMAGE validation")
VM_NAME=$(echo "$OUTPUT" | head -n 1 | awk '{print $3}')

echo "$(date) - Created $VM_NAME"

az_vm_ssh \
    "$VM_NAME" \
    'echo "deb http://deb.debian.org/debian/ testing main contrib non-free" | sudo tee /etc/apt/sources.list.d/testing.list'

APT_PREFS="Package: *
Pin: release a=stable
Pin-Priority: 500

Package: *
Pin: release a=testing
Pin-Priority: 400"
az_vm_ssh "$VM_NAME" "echo '$APT_PREFS' | sudo tee /etc/apt/preferences.d/testing.pref"

az_vm_ssh "$VM_NAME" "sudo apt-get update"

az_vm_ssh "$VM_NAME" "sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -t testing cloud-init=20.2-2"

az_vm_ssh "$VM_NAME" "sudo cloud-init clean --logs --reboot" || true

sleep 60

az_vm_ssh "$VM_NAME" "cloud-init status --long"
az_vm_ssh "$VM_NAME" "dpkg-query --show cloud-init"
az_vm_ssh "$VM_NAME" "sudo cat /run/cloud-init/result.json"
if az_vm_ssh "$VM_NAME" "grep Trace /var/log/cloud-init.log"; then
    echo "Potential issue in cloud-init.log"
    exit 1
fi
az_vm_ssh "$VM_NAME" "systemd-analyze"
az_vm_ssh "$VM_NAME" "systemd-analyze blame"
az_vm_ssh "$VM_NAME" "cloud-init analyze show"
az_vm_ssh "$VM_NAME" "cloud-init analyze blame"
echo 'Networking config'
az_vm_ssh "$VM_NAME" "cat $NETCFG"

az_vm_ssh "$VM_NAME" "dpkg-query --show cloud-init"
az_vm_ssh "$VM_NAME" "sudo hostname SRU-didnt-work"
az_vm_ssh "$VM_NAME" "sudo rm -f $NETCFG"
az_vm_ssh "$VM_NAME" "sudo cloud-init clean --logs --reboot" || true

sleep 60

ssh-keygen -f "$KNOWN_HOSTS" -R "$(full_dns_name $VM_NAME)"

az_vm_ssh "$VM_NAME" "cloud-init status --wait --long"
if az_vm_ssh "$VM_NAME" 'grep Trace /var/log/cloud-init\*'; then
    echo "Potential issue in cloud-init logs"
    exit 1
fi
az_vm_ssh "$VM_NAME" "sudo cat /run/cloud-init/result.json"
az_vm_ssh "$VM_NAME" "sudo systemd-analyze blame"
az_vm_ssh "$VM_NAME" "cloud-init analyze show"
az_vm_ssh "$VM_NAME" "cloud-init analyze blame"
echo 'After upgrade Networking config'
az_vm_ssh "$VM_NAME" "cat $NETCFG"
az_vm_ssh "$VM_NAME" "cloud-init query --format \"'cloud-region: {{cloud_name}}-{{region}}'\""
echo 'Get cloud-id'
az_vm_ssh "$VM_NAME" "cloud-id"
az_vm_ssh "$VM_NAME" "cloud-init query --format \"'cloud-region: {{cloud_name}}-{{ds.meta_data.imds.compute.location}}'\""
echo 'Validating whether metadata is being updated per boot LP:1819913. Expect last log to be Sytem Boot'
az_vm_ssh "$VM_NAME" "sudo reboot" || true
sleep 30
az_vm_ssh "$VM_NAME" "cloud-init status --wait --long"
echo 'After reboot'
az_vm_ssh "$VM_NAME" "grep 'Update datasource' /var/log/cloud-init.log"

echo "$(date) - Succeeded SRU validation"
