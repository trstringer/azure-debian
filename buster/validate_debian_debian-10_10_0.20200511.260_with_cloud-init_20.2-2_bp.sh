#!/bin/bash -i

set -e

validate_image () {
    local IMAGE="$1"

    local OUTPUT=$(az_vm_create "$IMAGE" "$IMAGE validation")
    local VM_NAME=$(echo "$OUTPUT" | head -n 1 | awk '{print $3}')
    echo "$(date) - Created $VM_NAME"

    echo "$(date) - Current apt sources..."
    az_vm_ssh "$VM_NAME" 'grep -vE "^$|#" /etc/apt/sources.list'

    az_vm_ssh \
        "$VM_NAME" \
        'echo "deb http://deb.debian.org/debian/ buster-backports main contrib non-free" | sudo tee /etc/apt/sources.list.d/backports.list'

    local APT_PREFS="Package: *
    Pin: release a=stable
    Pin-Priority: 500

    Package: *
    Pin: release a=buster-backports
    Pin-Priority: 400"
    az_vm_ssh "$VM_NAME" "echo '$APT_PREFS' | sudo tee /etc/apt/preferences.d/cloudinit.pref"

    az_vm_ssh "$VM_NAME" "sudo apt-get update"

    echo "$(date) - Installing cloud-init"
    az_vm_ssh "$VM_NAME" "sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -t buster-backports cloud-init=20.2-2~bpo10+1"

    echo "$(date) - Creating image from $VM_NAME"
    az_image_create_from_vm "$VM_NAME"

    echo "$(date) - Creating VM from image $VM_NAME"
    local OUTPUT=$(az_vm_create "$VM_NAME" "$IMAGE validation vm")
    local NEW_VM_NAME=$(echo "$OUTPUT" | head -n 1 | awk '{print $3}')
    echo "$(date) - Created $NEW_VM_NAME"

    echo "$(date) - Validating cloud-init was used for provisioning"
    if az_vm_ssh "$NEW_VM_NAME" "cat /var/log/waagent.log" | grep "Running CloudInit provisioning handler"; then
        echo "$(date) - Success validating $IMAGE ran cloud-init"
    else
        echo "$(date) - FAILURE validating $IMAGE ran cloud-init"
        exit 1
    fi

    echo "$(date) - Validating that cloud-init service is in a good state"
    if az_vm_ssh "$NEW_VM_NAME" "systemctl show --property Result cloud-init" | grep "Result=success"; then
        echo "$(date) - Success validating that cloud-init resulted in success"
    else
        echo "$(date) - FAILURE validating that cloud-init resulted in success"
        exit 1
    fi
}

validate_image "debian:debian-10:10:0.20200511.260"
